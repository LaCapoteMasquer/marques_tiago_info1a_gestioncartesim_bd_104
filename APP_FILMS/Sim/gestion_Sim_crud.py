"""
    Fichier : gestion_Sim_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les Sim.
"""
from flask import render_template, flash, request
from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.erreurs import msg_erreurs
from APP_FILMS.erreurs.exceptions import *

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /Sim_afficher
    
    Test : ex : http://127.0.0.1:5005/Sim_afficher
    
    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                ID_Sim_sel = 0 >> tous les Sim.
                ID_Sim_sel = "n" affiche le genre dont l'id est "n"
"""


@obj_mon_application.route("/Sim_afficher/<string:order_by>/<int:ID_Sim_sel>", methods=['GET', 'POST'])
def Sim_afficher(order_by, ID_Sim_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion Sim ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionSim {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and ID_Sim_sel == 0:
                    strsql_Sim_afficher = """SELECT * FROM t_sim ORDER BY ID_Sim ASC"""
                    mc_afficher.execute(strsql_Sim_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_genre"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du genre sélectionné avec un nom de variable
                    valeur_ID_Sim_selected_dictionnaire = {"value_ID_Sim_selected": ID_Sim_sel}
                    strsql_Sim_afficher = """SELECT * FROM t_sim  WHERE ID_Sim = %(value_ID_Sim_selected)s"""

                    mc_afficher.execute(strsql_Sim_afficher, valeur_ID_Sim_selected_dictionnaire)
                else:
                    strsql_Sim_afficher = """SELECT ID_Sim, intitule_genre FROM t_genre ORDER BY ID_Sim DESC"""

                    mc_afficher.execute(strsql_Sim_afficher)

                data_Sim = mc_afficher.fetchall()

                print("Données : ", data_Sim, " Type : ", type(data_Sim))

                # Différencier les messages si la table est vide.
                if not data_Sim and ID_Sim_sel == 0:
                    flash("""La table "t_sim" est vide. !!""", "warning")
                elif not data_Sim and ID_Sim_sel > 0:
                    # Si l'utilisateur change l'ID_Sim dans l'URL et que le genre n'existe pas,
                    flash(f"Le genre demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_genre" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Données Sim affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur}")
            raise Exception(f"RGG Erreur générale. {erreur}")
            raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("Sim/Sim_afficher.html", data=data_Sim)
